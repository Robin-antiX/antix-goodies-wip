��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  .   q  +   �     �  %   �       !   (     J     Q     k     {     �  #   �  &   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-29 23:41-0600
PO-Revision-Date: 2018-09-23 15:53+0300
Last-Translator: En Kerro <inactive+ekeimaja@transifex.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 (Yhdistäminen valittuun laitteeseen onnistui) (ei voitu yhdistää valittuun laitteeseen) Nykyinen oletus on %s Äänikortteja/laitteita ei löytynyt Vain yksi äänikortti löytyi. Ole hyvä ja valitse äänikortti Lopeta Äänikortiksi valittu %s Äänen testaus Testi epäonnistui Testi onnistui Kokeillaan ääntä 6 sekunnin ajan Haluatko kokeilla äänen toimivuutta? 