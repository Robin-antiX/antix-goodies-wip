��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  �  =     �     �  �     =  �  K   �	  '   
  -   ?
     m
     �
  N   �
     �
     �
                                
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:32+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 LEGG TIL valgt program Programmets .desktop-fil Velg (eller dra og slipp til feltet under) .desktop-filen du vil legge til den personlige menyen\ ELLER velg et annet alternativ FPM har ingen grafisk måte å la brukere flytte ikoner rundt på eller slette vilkårlige ikoner.\nHvis du velger OK vil oppsettsfilen for den personlige menyen åpnes for redigering.\nHvert menyikon identifiseres med ei linje som starter med «prog» etterfulgt av programmets navn, ikonets plassering og selve programfila.\nFlytt eller slett hele linja som viser til menyoppføringen.\nMerknad: Linjer som starter med # er kommentarer og ignoreres.\nTomme linjer er tillatt.\nLagre endringer og start IceWM på nytt.\nSiste endring kan angres med FPMs gjenopprett-knapp. FTM er programmert til å alltid beholde 1 linje i den personlige menyfila. Rask personlig menybehandling for IceWM Ingen endringer ble utført. Velg et program. ORGANISER oppføringer FJERN siste oppføring Dette vil slette den siste oppføringen i den personlige menyen. Er du sikker? ANGRE siste endring Advarsel 